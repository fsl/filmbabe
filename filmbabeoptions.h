/*  FilmbabeOptions.h

    Mark Woolrich, FMRIB Image Analysis Group

    Copyright (C) 1999-2000 University of Oxford  */

/*  CCOPYRIGHT  */

#if !defined(FilmbabeOptions_h)
#define FilmbabeOptions_h

#include <string>
#include <iostream>
#include <fstream>
#include <stdlib.h>
#include <stdio.h>
#include "utils/options.h"
#include "utils/log.h"

namespace Filmbabe {

class FilmbabeOptions {
 public:
  static FilmbabeOptions& getInstance();
  ~FilmbabeOptions() { delete gopt; }

  Utilities::Option<bool> verbose;
  Utilities::Option<int> debuglevel;
  Utilities::Option<bool> timingon;
  Utilities::Option<bool> help;
  Utilities::Option<bool> flobsprioroff;
  Utilities::Option<std::string> datafile;
  Utilities::Option<std::string> maskfile;
  Utilities::Option<std::string> designfile;
  Utilities::Option<std::string> flobsregressorsfile;
  Utilities::Option<std::string> flobsdir;
  Utilities::Option<std::string> priorcovarfile;
  Utilities::Option<std::string> priormeanfile;
  Utilities::Option<std::string> logdir;
  Utilities::Option<int> niters;
  Utilities::Option<float> tarmrfprec;
  Utilities::Option<bool> tarard;
  Utilities::Option<int> ntracesamps;
  Utilities::Option<int> ntar;

  void parse_command_line(int argc, char** argv, Utilities::Log& logger);

 private:
  FilmbabeOptions();
  const FilmbabeOptions& operator=(FilmbabeOptions&);
  FilmbabeOptions(FilmbabeOptions&);

  Utilities::OptionParser options;

  static FilmbabeOptions* gopt;

};

 inline FilmbabeOptions& FilmbabeOptions::getInstance(){
   if(gopt == NULL)
     gopt = new FilmbabeOptions();

   return *gopt;
 }

 inline FilmbabeOptions::FilmbabeOptions() :
   verbose(std::string("-V,--verbose"), false,
	   std::string("switch on diagnostic messages"),
	   false, Utilities::no_argument),
   debuglevel(std::string("--db,--debug,--debuglevel"), 0,
		       std::string("set debug level"),
		       false, Utilities::requires_argument),
   timingon(std::string("--to,--timingon"), false,
		       std::string("turn timing on"),
		       false, Utilities::no_argument),
   help(std::string("-h,--help"), false,
		    std::string("display this message"),
		    false, Utilities::no_argument),
   flobsprioroff(std::string("--fpo,--flobsprioroff"), false,
		    std::string("Turn FLOBS prior off"),
		    false, Utilities::no_argument),
   datafile(std::string("--df,--datafile"), std::string(""),
			  std::string("data file"),
		     true, Utilities::requires_argument),
   maskfile(std::string("-m,--mask"), std::string(""),
	    std::string("mask file"),
	    true, Utilities::requires_argument),
   designfile(std::string("-d,--dm,--designfile"), std::string(""),
	      std::string("design matrix file"),
	      true, Utilities::requires_argument),
   flobsregressorsfile(std::string("--frf"), std::string(""),
	      std::string("file indicating which regressors belong to which original ev design matrix file (a -1 label indicates a non-flobs regerssor)"),
	      true, Utilities::requires_argument),
   flobsdir(std::string("--fd"), std::string(""),
	      std::string("flobs directory, when using flobs constraints need this or to explicitly specify --pcf and--pmf"),
	    false, Utilities::requires_argument),
   priorcovarfile(std::string("--pcf,--priorcovarfile"), std::string(""),
	      std::string("priorcovar matrix file"),
	      false, Utilities::requires_argument),
   priormeanfile(std::string("--pmf,--priormeanfile"), std::string(""),
	      std::string("priormean matrix file"),
	      false, Utilities::requires_argument),
   logdir(std::string("-l,--ld,--logdir"), std::string("logdir"),
	  std::string("log directory"),
	  false, Utilities::requires_argument),
   niters(std::string("--ni"), 5,
	  std::string("Num pf VB iterations (default is 5)"),
	  false, Utilities::requires_argument),
   tarmrfprec(std::string("--tmp,--tarmrfprec"), -1,
	      std::string("MRF precision to impose on temporal AR maps, default is -1 for a proper full Bayes approach"),
	      false, Utilities::requires_argument),
   tarard(std::string("--tarard"), false,
	  std::string("Impose ARD/MRF on temporal AR"),
	  false, Utilities::no_argument),
   ntracesamps(std::string("--nts,--ntracesamps"), 0,
	       std::string("No of samples to take to estimate trace (default is 0, which uses only diagonal elements of the precision matrix to estimate trace)"),
	       false, Utilities::requires_argument),
   ntar(std::string("--ntar"), 3,
	std::string("Order of temporal AR (default is 3)"),
	false, Utilities::requires_argument),
   options("filmbabe", "filmbabe -df <filename>\n filmbabe --verbose\n")
 {
     try {
       options.add(verbose);
       options.add(debuglevel);
       options.add(timingon);
       options.add(help);
       options.add(flobsprioroff);
       options.add(datafile);
       options.add(maskfile);
       options.add(designfile);
       options.add(flobsregressorsfile);
       options.add(flobsdir);
       options.add(priorcovarfile);
       options.add(priormeanfile);
       options.add(logdir);
       options.add(niters);
       options.add(tarmrfprec);
       options.add(tarard);
       options.add(ntracesamps);
       options.add(ntar);

     }
     catch(Utilities::X_OptionError& e) {
       options.usage();
       std::cerr << std::endl << e.what() << std::endl;
     }
     catch(std::exception &e) {
       std::cerr << e.what() << std::endl;
     }

   }
}

#endif
