include ${FSLCONFDIR}/default.mk

PROJNAME = filmbabe
SCRIPTS  = filmbabescript
XFILES   = filmbabe halfcosbasis
TCLFILES = *.tcl *.ppm
RUNTCLS  = Make_flobs
LIBS     = -lfsl-miscplot -lfsl-miscpic -lfsl-newimage \
           -lfsl-miscmaths -lfsl-NewNifti -lfsl-cprob -lfsl-znz \
           -lfsl-utils -lgdc -lgd -lpng
OBJS     = halfcosbasisoptions.o filmbabe_manager.o \
           filmbabeoptions.o filmbabe_vb_flobs.o

all: ${XFILES}

filmbabe: ${OBJS} filmbabe.o
	${CXX} ${CXXFLAGS} -o $@ $^ ${LDFLAGS}

halfcosbasis: halfcosbasisoptions.o halfcosbasis.o
	${CXX} ${CXXFLAGS} -o $@ $^ ${LDFLAGS}
