/*  filmbabe.cc

    Mark Woolrich - FMRIB Image Analysis Group

    Copyright (C) 2004 University of Oxford  */

/*  CCOPYRIGHT  */

#include <iostream>
#include <fstream>
#include <iomanip>
#include <sstream>

#include <string>
#include <math.h>

#include "utils/log.h"
#include "utils/tracer_plus.h"
#include "armawrap/newmat.h"

#include "filmbabeoptions.h"
#include "filmbabe_manager.h"

using namespace std;
using namespace Utilities;
using namespace NEWMAT;
using namespace Filmbabe;

int main(int argc, char *argv[])
{
  try{

    // Setup logging:
    Log& logger = LogSingleton::getInstance();

    // parse command line - will output arguments to logfile
    FilmbabeOptions& opts = FilmbabeOptions::getInstance();
    opts.parse_command_line(argc, argv, logger);

    //Tracer_Plus::setinstantstackon();

    if(opts.debuglevel.value()==1)
      Tracer_Plus::setrunningstackon();

    if(opts.timingon.value())
      Tracer_Plus::settimingon();

    // setup Mcmc object
    Filmbabe_Manager filmbabe_man(opts);
    filmbabe_man.setup();
    filmbabe_man.run();
    filmbabe_man.save();

    if(opts.timingon.value())
      Tracer_Plus::dump_times(logger.getDir());

    cout << "Log directory was: " << logger.getDir() << endl;

  }
  catch(Exception& e)
    {
      cerr << endl << e.what() << endl;
      return 1;
    }
  catch(X_OptionError& e)
    {
      cerr << endl << e.what() << endl;
      return 1;
    }

  return 0;
}
