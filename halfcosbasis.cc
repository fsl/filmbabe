/*  halfcosbasis.cc

    Mark Woolrich - FMRIB Image Analysis Group

    Copyright (C) 2004 University of Oxford  */

/*  CCOPYRIGHT  */

#include <iostream>
#include <fstream>
#include <iomanip>
#include <sstream>
#include <string>
#include <math.h>

#include "utils/log.h"
#include "utils/tracer_plus.h"
#include "armawrap/newmat.h"
#include "miscmaths/miscmaths.h"
#include "miscmaths/miscprob.h"
#include "libvis/miscplot.h"

#include "halfcosbasisoptions.h"

using namespace std;
using namespace Utilities;
using namespace NEWMAT;
using namespace MISCMATHS;
using namespace MISCPLOT;
using namespace Halfcosbasis;

inline float halfcos(float t,float ybot,float ytop,float xleft,float xright,int flipud)
{
  float y;

  if(xright>xleft && t>xleft && t<=xright)
    y=((ytop-ybot)/2.0*flipud*std::cos(2*M_PI*(t-xleft)/((xright-xleft)*2))+(ytop-ybot)/2+ybot);
  else
    y=0;

  return y;
}

float halfcos_hrf(float t,  const ColumnVector& halfcosparams)
//float m1,float m2,float m3,float m4,float m5,float c1,float c2,float c3)
{
  float m1 = halfcosparams(1);
  float m2 = halfcosparams(2);
  float m3 = halfcosparams(3);
  float m4 = halfcosparams(4);
  float m5 = halfcosparams(5);
  float c1 = halfcosparams(6);
  float c2 = halfcosparams(7);
  float c3 = halfcosparams(8);

  float y=halfcos(t,-1*c1,0,0,m1,1);
  y+=halfcos(t,-1*c1,1,m1,m1+m2,-1);
  y+=halfcos(t,-c2,1,m1+m2,m1+m2+m3,1);
  y+=halfcos(t,-c2,c3,m1+m2+m3,m1+m2+m3+m4,-1);
  y+=halfcos(t,0,c3,m1+m2+m3+m4,m1+m2+m3+m4+m5,1);

  return y;
}


ReturnMatrix halfcos_hrf(const ColumnVector& t, const ColumnVector& halfcosparams)
{
  Tracer_Plus trace("halfcos_hrf");

  int T = t.Nrows();
  ColumnVector y(T);
  y = 0;

  for(int i = 1; i <= T; i++)
  {
    y(i) = halfcos_hrf(t(i),halfcosparams);
  }

  y.Release();
  return y;
}

ReturnMatrix samplehalfcosparams(const Matrix& halfcosparamsranges)
{
  Tracer_Plus trace("samplehalfcosparams");

  int nparams = halfcosparamsranges.Nrows();

  ColumnVector halfcosparams(nparams);
  halfcosparams = 0;

  for(int i = 1; i <= nparams; i++)
  {
    halfcosparams(i) = unifrnd(1,1,halfcosparamsranges(i,1),halfcosparamsranges(i,2)).AsScalar();
  }

  halfcosparams.Release();
  return halfcosparams;
}

int main(int argc, char *argv[])
{
  try{

    // Setup logging:
    Log& logger = LogSingleton::getInstance();

    // parse command line - will output arguments to logfile
    HalfcosbasisOptions& opts = HalfcosbasisOptions::getInstance();
    opts.parse_command_line(argc, argv, logger);

    //Tracer_Plus::setinstantstackon();

    Matrix halfcosparamranges = read_ascii_matrix(opts.halfcosparamrangesfile.value());

    if(opts.verbose.value())
      cout << halfcosparamranges << endl;

    if(halfcosparamranges.Nrows() != 8 || halfcosparamranges.Ncols() != 2)
    {
      throw Exception("Invalid halfcosparamranges file");
    }

    int T = int(opts.nsecs.value()/opts.res.value());

    if(opts.verbose.value())
    {
      cout << T << endl;
      cout << opts.nsecs.value() << endl;
      cout << opts.res.value() << endl;
      cout << opts.nhrfsamps.value() << endl;
    }

    // setup time vector
    ColumnVector t(T);
    t = 0;
    for(int i = 1; i <= T; i++)
    {
      t(i) = i*opts.res.value();
    }

    if(opts.verbose.value())
      cout << "Generating HRF samples" << endl;
    Matrix hrfsamps(T,opts.nhrfsamps.value());
    hrfsamps = 0;

    for(int i = 1; i <= opts.nhrfsamps.value(); i++)
    {
      ColumnVector halfcosparams = samplehalfcosparams(halfcosparamranges);
      hrfsamps.Column(i) = halfcos_hrf(t,halfcosparams);
    }

    if(opts.verbose.value())
      cout << "Finished generating HRF samples" << endl;

    if(opts.verbose.value())
      cout << "Performing SVD" << endl;

    DiagonalMatrix eigenvals;
    Matrix eigenvecs;

    try
    {
      //SymmetricMatrix corr;
      //corr << hrfsamps*hrfsamps.t()/opts.nhrfsamps.value();
      //EigenValues(corr, eigenvals, eigenvecs);

      // use svd instead of eigenvalues - uses much less memory
      if (hrfsamps.Ncols()>hrfsamps.Nrows())  // Ncols has to be <= Nrows for SVD()
      {
        Matrix grot;
        SVD(hrfsamps.t(), eigenvals, grot, eigenvecs);
      }
      else
        SVD(hrfsamps, eigenvals, eigenvecs);
      eigenvals = (eigenvals * eigenvals).Reverse(); // square and reverse order
      for(int i = 1; i <= eigenvecs.Nrows(); i++)
        eigenvecs.Row(i)=eigenvecs.Row(i).Reverse(); // reverse order of columns
    }
    catch(Exception& e)
    {
      if(opts.verbose.value())
        cerr << endl << e.what() << endl;

      throw e;
    }

    if(opts.verbose.value())
    {
      cout << "Finished SVD" << endl;
      write_ascii_matrix(eigenvecs, LogSingleton::getInstance().appendDir("hrfeigenvecs.txt"));
      write_ascii_matrix(diag(eigenvals), LogSingleton::getInstance().appendDir("hrfeigenvals.txt"));
    }

    // need to flip cos of the way EigenValues does its output
    eigenvecs = eigenvecs.Reverse();
    eigenvals = eigenvals.Reverse();

    Matrix basisfns = eigenvecs.Columns(1,opts.nbfs.value());

    // need to flip (in time) basis function time series cos of the way EigenValues does its output
    for(int i=1; i<=opts.nbfs.value(); i++)
    {
      basisfns.Column(i) = basisfns.Column(i).Reverse();
    }

    // find max and min of biggest component and flip all (in amp) if abs(min)>abs(max)
    if(abs(basisfns.Column(1).Minimum())>abs(basisfns.Column(1).Maximum()))
    {
      basisfns = -basisfns;
    }

    // Output hrf samples
    write_ascii_matrix(hrfsamps, LogSingleton::getInstance().appendDir("hrfsamps.txt"));
    miscplot newplot;
    newplot.add_xlabel("time (secs)");
    newplot.set_xysize(610,300);
    newplot.timeseries(hrfsamps.t(), LogSingleton::getInstance().appendDir("hrfsamps"), "HRF Samples", opts.res.value(),400,3,0,false);

    // output basis functions
    write_ascii_matrix(basisfns, LogSingleton::getInstance().appendDir("hrfbasisfns.txt"));
    miscplot newplot2;
    for(int i = 1; i <= opts.nbfs.value(); i++)
    {
      newplot2.add_label(string("Basis fn ")+num2str(i));
    }
    newplot2.add_xlabel("time (secs)");
    newplot2.set_xysize(600,300);
    newplot2.timeseries(basisfns.t(), LogSingleton::getInstance().appendDir("hrfbasisfns"), "HRF Basis Functions", opts.res.value(),400,3,0,false);

    // find number of eigenvalues to display:
    float sumeigs = eigenvals.Sum();
    float runsum = 0.0;
    int numeigs = 1;

    if(opts.verbose.value())
      cout << sumeigs << endl;

    for(; numeigs < 100; numeigs++)
    {
      runsum += eigenvals(numeigs,numeigs);

      if(runsum>0.995*sumeigs) break;
    }

    if(opts.verbose.value())
    {
      cout << runsum << endl;
      cout << numeigs << endl;
    }

    miscplot newplot3;
    newplot3.add_xlabel("basis function number");
    newplot3.set_xysize(300,300);
    newplot3.timeseries((diag(eigenvals).Rows(1,numeigs)/eigenvals(1,1)).t(), LogSingleton::getInstance().appendDir("eigenvalues"), "Normalised Eigenvalues (99.5% of variance)", 0,400,3,0,false);

    if(opts.verbose.value())
      std::cout << "Normalising HRF samples and basis set" << std::endl;

    //     // remove mean and variance from basis functions and hrf samples
    //     for(int i = 1; i <= opts.nbfs.value(); i++)
    //       {
    // 	ColumnVector tmpcol = basisfns.Column(i);
    // 	basisfns.Column(i) = (tmpcol - mean(tmpcol).AsScalar())/stdev(tmpcol).AsScalar();
    //       }

    //     for(int i = 1; i <= opts.nhrfsamps.value(); i++)
    //       {
    // 	ColumnVector tmpcol = hrfsamps.Column(i);
    // 	hrfsamps.Column(i) = (tmpcol - mean(tmpcol).AsScalar())/stdev(tmpcol).AsScalar();
    //       }

    // remove mean and variance from basis functions and hrf samples
    for(int i = 1; i <= opts.nbfs.value(); i++)
    {
      ColumnVector tmpcol = basisfns.Column(i);
      basisfns.Column(i) = (tmpcol - mean(tmpcol).AsScalar());
    }

    for(int i = 1; i <= opts.nhrfsamps.value(); i++)
    {
      ColumnVector tmpcol = hrfsamps.Column(i);
      hrfsamps.Column(i) = (tmpcol - mean(tmpcol).AsScalar());
    }

    if(opts.verbose.value())
      std::cout << "Regressing HRF samples onto basis set" << std::endl;

    Matrix paramsamps = pinv(basisfns)*hrfsamps;
    //write_ascii_matrix(paramsamps, LogSingleton::getInstance().appendDir("paramsamps"));

    if(opts.verbose.value())
      std::cout << "Fitting MVN parameter constraints" << std::endl;

    ColumnVector priormeans = mean(paramsamps.t()).t();
    Matrix priorcovars = cov(paramsamps.t());

    // output constraints
    write_vest(priormeans, LogSingleton::getInstance().appendDir("priormeans.mat"));
    write_vest(priorcovars, LogSingleton::getInstance().appendDir("priorcovars.mat"));

    if(opts.verbose.value())
    {
      std::cout << priormeans << std::endl;
      std::cout << priorcovars << std::endl;
    }

    if(opts.debuglevel.value()==1)
      Tracer_Plus::setrunningstackon();

    if(opts.timingon.value())
      Tracer_Plus::settimingon();

    if(opts.timingon.value())
      Tracer_Plus::dump_times(logger.getDir());

    cout << "Log directory was: " << logger.getDir() << endl;

  }
  catch(Exception& e)
  {
    cerr << endl << e.what() << endl;
    return 1;
  }
  catch(X_OptionError& e)
  {
    cerr << endl << e.what() << endl;
    return 1;
  }

  return 0;
}
