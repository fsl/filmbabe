/*  filmbabe_vb_flobs.h

    Mark Woolrich, FMRIB Image Analysis Group

    Copyright (C) 1999-2000 University of Oxford  */

/*  CCOPYRIGHT  */

#if !defined(filmbabe_vb_flobs_h)
#define filmbabe_vb_flobs_h

#include <iostream>
#include <fstream>
#include <vector>

#include "utils/tracer_plus.h"
#include "armawrap/newmat.h"
#include "newimage/newimageall.h"
#include "miscmaths/sparse_matrix.h"

#include "filmbabeoptions.h"
#include "connected_offset.h"



namespace Filmbabe {

  class Voxel
    {
    public:
      int x;
      int y;
      int z;

      Voxel(int px, int py, int pz) : x(px),y(py),z(pz){}

      bool operator==(const Voxel& r) const
      {return (x==r.x && y==r.y && z==r.z);}

      bool is_neighbour(const Voxel& r) const
	{return ((x==r.x-1 && y==r.y && z==r.z)||
		 (x==r.x+1 && y==r.y && z==r.z)||
		 (x==r.x && y==r.y-1 && z==r.z)||
		 (x==r.x && y==r.y+1 && z==r.z)||
		 (x==r.x && y==r.y && z==r.z-1)||
		 (x==r.x && y==r.y && z==r.z+1)) ;}
    };

  class Filmbabe_Vb_Flobs
    {
    public:

      // Constructor
      Filmbabe_Vb_Flobs(const NEWIMAGE::volume4D<float>& pdata, const NEWIMAGE::volume<int>& pmask, const NEWMAT::Matrix& pdesignmatrix, const NEWMAT::ColumnVector& pflobsregressors, const NEWIMAGE::volume4D<float>& plocalweights, const std::vector<MISCMATHS::Connected_Offset>& pconnected_offsets, int pnum_superthreshold);

      // setup
      void setup();

      // run
      void run();

      // save data to logger dir
      void save() ;

      // Destructor
      virtual ~Filmbabe_Vb_Flobs(){}

    private:

      Filmbabe_Vb_Flobs();
      const Filmbabe_Vb_Flobs& operator=(Filmbabe_Vb_Flobs&);
      Filmbabe_Vb_Flobs(Filmbabe_Vb_Flobs&);

      void process_flobsregressors();
      void update_Beta();
      void update_A();
      void update_phiA();
      void update_phie();
      void update_phiBeta();

      int xsize;
      int ysize;
      int zsize;
      int ntpts;

      // num flobs Original evs
      int nflobsevs;

      // num basis functions for each flobs Original evs (is the same for all Original evs)
      int nbfs;

      // num of non flobs evs
      int nnonflobsevs;

      // total number of actual real evs in design matrix
      int nrealevs;

      const NEWIMAGE::volume4D<float>& data;
      const NEWIMAGE::volume<int>& mask;
      NEWMAT::Matrix designmatrix;
      const NEWMAT::ColumnVector& flobsregressors;

      NEWMAT::Matrix QtXXt;

      const NEWIMAGE::volume4D<float>& localweights;
      const std::vector<MISCMATHS::Connected_Offset>& connected_offsets;

      NEWIMAGE::volume<int> indices;
      std::vector<Voxel> voxels;

      NEWMAT::Matrix Y;

      // Prior distribution params
      // m_Beta_0(e)
      NEWMAT::ColumnVector m_Beta_0_global;
      NEWMAT::SymmetricMatrix lambda_Beta_0_global;

      std::vector<NEWMAT::ColumnVector> m_Beta_0;
      std::vector<NEWMAT::SymmetricMatrix> lambda_Beta_0;

      MISCMATHS::SparseMatrix D;

      NEWMAT::ColumnVector trace_ilambdaDA;
      std::vector<NEWMAT::ColumnVector> trace_ilambdaBeta;
      NEWMAT::ColumnVector trace_ilambdaXXt;
      std::vector<NEWMAT::ColumnVector> diag_ilambdaA;

      // Posterior distribution params

      // q(Beta)~MVN(m_Beta,lambda_Beta);
      // m_Beta[i]((bf-1)*nevs+e)
      std::vector<NEWMAT::ColumnVector> m_Beta;
      std::vector<NEWMAT::SymmetricMatrix> ilambda_Beta;

      // q(A_p)~MVN(m_A_p,lambda_A_p);
      // m_A[p](i)
      std::vector<NEWMAT::ColumnVector> m_A;

      // q(phi_Beta)~Ga(b_Beta,c_Beta);
      // gam_Beta = gam(c_Beta+1)/gam(c_Beta)
      // gam_Beta[i](e)
      std::vector<NEWMAT::ColumnVector> gam_Beta;

      // q(phi_A_p)~Ga(b_A_p,c_A_p);
      // gam_A_p = gam(c_A_p+1)/gam(c_A_p)
      NEWMAT::ColumnVector gam_A;

      // q(phi_e)~Ga(b_e,c_e);
      // gam_e = gam(c_e+1)/gam(c_e)
      NEWMAT::ColumnVector gam_e;

      int num_superthreshold;

      int maxnumneighs;
      int ntar;

      int count;

      std::vector<MISCMATHS::SparseMatrix> ilambdaDA;
      std::vector<MISCMATHS::SparseMatrix> ilambdaA;

      int niters;

      NEWMAT::Matrix realevcoord;

      std::vector<NEWMAT::Matrix> QemReCquad;
      std::vector<NEWMAT::RowVector> Re;
      std::vector<NEWMAT::Matrix> Qe;
      NEWMAT::Matrix Q;
      NEWMAT::Matrix designmatrixQ;

      std::vector<float> gamAhist;
    };

}
#endif
