/*  FilmbabeOptions.cc

    Mark Woolrich, FMRIB Image Analysis Group

    Copyright (C) 1999-2000 University of Oxford  */

/*  CCOPYRIGHT  */

#include <iostream>
#include <fstream>
#include <stdlib.h>
#include <stdio.h>

#include "utils/log.h"
#include "utils/tracer_plus.h"
#include "armawrap/newmat.h"

#include "filmbabeoptions.h"

using namespace std;
using namespace Utilities;
using namespace NEWMAT;

namespace Filmbabe {

FilmbabeOptions* FilmbabeOptions::gopt = NULL;

void FilmbabeOptions::parse_command_line(int argc, char** argv, Log& logger)
{
  Tracer_Plus("FilmbabeOptions::parse_command_line");

  // do once to establish log directory name
  for(int a = options.parse_command_line(argc, argv); a < argc; a++);

  // setup logger directory
  logger.makeDir(logdir.value());

  cout << "Log directory is: " << logger.getDir() << endl;

  // do again so that options are logged
  for(int a = 0; a < argc; a++)
    logger.str() << argv[a] << " ";
  logger.str() << endl << "---------------------------------------------" << endl << endl;

  if(help.value() || ! options.check_compulsory_arguments())
    {
      options.usage();
      throw Exception("Not all of the compulsory arguments have been provided");
    }
}

}
