/*  filmbabe_manager.cc

    Mark Woolrich, FMRIB Image Analysis Group

    Copyright (C) 1999-2000 University of Oxford  */

/*  CCOPYRIGHT  */


#include "utils/log.h"
#include "miscmaths/miscmaths.h"
#include "newimage/newimageall.h"
#include "utils/tracer_plus.h"
#include "filmbabe_manager.h"

using namespace std;
using namespace Utilities;
using namespace MISCMATHS;
using namespace NEWIMAGE;

namespace Filmbabe {

  Filmbabe_Manager::~Filmbabe_Manager()
  {
    delete filmbabe_vb_flobs;
  }

  void Filmbabe_Manager::setup()
  {
    Tracer_Plus trace("Filmbabe_Manager::setup");

    if(FilmbabeOptions::getInstance().debuglevel.value()==2)
      {
	cout << "******************************************" << endl
	     << "SETUP" << endl << "******************************************"
	     << endl;
      }

    cout << "datafile =" << opts.datafile.value() << endl;
    read_volume4D(data, opts.datafile.value());

    cout << "maskfile =" << opts.maskfile.value() << endl;
    copybasicproperties(data[0],mask);
    read_volume(mask, opts.maskfile.value());

    cout << "designfile =" << opts.designfile.value() << endl;
    designmatrix = read_vest(opts.designfile.value()).t();

    cout << "flobsregressorsfile =" << opts.flobsregressorsfile.value() << endl;
    flobsregressors = read_ascii_matrix(opts.flobsregressorsfile.value());

    cout << flobsregressors.t() << endl;
    cout << designmatrix.Nrows() << endl;
    cout << designmatrix.Ncols() << endl;

    localweights.reinitialize(data.xsize(),data.ysize(),data.zsize(),6);
    localweights = 0;
    int num_superthreshold = 0;

    for(int x = 0; x < data.xsize(); x++)
      for(int y = 0; y < data.ysize(); y++)
	for(int z = 0; z < data.zsize(); z++)
	  if(mask(x,y,z))
	    {
	      num_superthreshold++;

	      int xi,yi,zi;
	      for(unsigned int i = 0; i < connected_offsets.size(); i++)
		{
		  xi = x+connected_offsets[i].x;
		  yi = y+connected_offsets[i].y;
		  zi = z+connected_offsets[i].z;

		  if(mask(xi,yi,zi))
		    {
		      localweights(x,y,z,connected_offsets[i].ind) = 1;
		    }
		}
	    }

    cout << num_superthreshold << endl;

    filmbabe_vb_flobs = new Filmbabe_Vb_Flobs(data, mask, designmatrix, flobsregressors, localweights, connected_offsets, num_superthreshold);
    filmbabe_vb_flobs->setup();

  }

  void Filmbabe_Manager::run()
  {
    Tracer_Plus trace("Filmbabe_Manager::run");

    if(FilmbabeOptions::getInstance().debuglevel.value()==2)
      {
	cout << "******************************************" << endl
	     << "RUN" << endl << "******************************************"
	     << endl;
      }

    filmbabe_vb_flobs->run();

    cout << endl << "Finished" << endl;
  }

  void Filmbabe_Manager::save()
  {
    Tracer_Plus trace("Filmbabe_Manager::save");

    filmbabe_vb_flobs->save();

    // save other stuff
    save_volume(mask, LogSingleton::getInstance().appendDir("mask"));

    save_volume4D(data, LogSingleton::getInstance().appendDir("data"));

  }

}
