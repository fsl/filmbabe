/*  HalfcosbasisOptions.h

    Mark Woolrich, FMRIB Image Analysis Group

    Copyright (C) 1999-2000 University of Oxford  */

/*  CCOPYRIGHT  */

#if !defined(HalfcosbasisOptions_h)
#define HalfcosbasisOptions_h

#include <string>
#include <iostream>
#include <fstream>
#include <stdlib.h>
#include <stdio.h>
#include "utils/options.h"
#include "utils/log.h"

namespace Halfcosbasis {

class HalfcosbasisOptions {
 public:
  static HalfcosbasisOptions& getInstance();
  ~HalfcosbasisOptions() { delete gopt; }

  Utilities::Option<bool> verbose;
  Utilities::Option<int> debuglevel;
  Utilities::Option<bool> timingon;
  Utilities::Option<bool> help;
  Utilities::Option<std::string> logdir;
  Utilities::Option<std::string> halfcosparamrangesfile;
  Utilities::Option<int> nhrfsamps;
  Utilities::Option<int> nbfs;
  Utilities::Option<int> nsecs;
  Utilities::Option<float> res;

  void parse_command_line(int argc, char** argv, Utilities::Log& logger);

 private:
  HalfcosbasisOptions();
  const HalfcosbasisOptions& operator=(HalfcosbasisOptions&);
  HalfcosbasisOptions(HalfcosbasisOptions&);

  Utilities::OptionParser options;

  static HalfcosbasisOptions* gopt;

};

 inline HalfcosbasisOptions& HalfcosbasisOptions::getInstance(){
   if(gopt == NULL)
     gopt = new HalfcosbasisOptions();

   return *gopt;
 }

 inline HalfcosbasisOptions::HalfcosbasisOptions() :
   verbose(std::string("-V,--verbose"), false,
	   std::string("switch on diagnostic messages"),
	   false, Utilities::no_argument),
   debuglevel(std::string("--db,--debug,--debuglevel"), 0,
		       std::string("set debug level"),
		       false, Utilities::requires_argument),
   timingon(std::string("--to,--timingon"), false,
		       std::string("turn timing on"),
		       false, Utilities::no_argument),
   help(std::string("-h,--help"), false,
		    std::string("display this message"),
		    false, Utilities::no_argument),
   logdir(std::string("-l,--ld,--logdir"), std::string("logdir"),
	  std::string("log directory"),
	  false, Utilities::requires_argument),
   halfcosparamrangesfile(std::string("--hcprf,--hf"), std::string(""),
	      std::string("Half cosine HRF parameter ranges file"),
	      true, Utilities::requires_argument),
   nhrfsamps(std::string("--nhs"), 1000,
	  std::string("Num of HRF samples to use (default is 1000)"),
	  false, Utilities::requires_argument),
   nbfs(std::string("--nbfs"), 3,
	  std::string("Num of HRF basis functions to use (default is 3)"),
	  false, Utilities::requires_argument),
   nsecs(std::string("--ns,--nsecs"), 40,
	  std::string("Num of secs (default is 40)"),
	  false, Utilities::requires_argument),
   res(std::string("--res"), 0.05,
	  std::string("Temporal resolution (default is 0.05)"),
	  false, Utilities::requires_argument),
   options("halfcosbasis","halfcosbasis --help")
 {
     try {
       options.add(verbose);
       options.add(debuglevel);
       options.add(timingon);
       options.add(logdir);
       options.add(halfcosparamrangesfile);
       options.add(nhrfsamps);
       options.add(nbfs);
       options.add(nsecs);
       options.add(res);
       options.add(help);
     }
     catch(Utilities::X_OptionError& e) {
       options.usage();
       std::cerr << std::endl << e.what() << std::endl;
     }
     catch(std::exception &e) {
       std::cerr << e.what() << std::endl;
     }

   }
}

#endif
